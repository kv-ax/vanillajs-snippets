# vanilla-js snippets
This is a collection of my snippets that I created for elements I cannot find online.
I use no dependencies and keep styling to only what's necessary to show how to create these elements without using massive libraries.

>>>
**Note**

I use [**pug**](//pugjs.org) to render **HTML**, [**stylus**](//stylus-lang.com/) to render **css**, and [**coffeescript**](//coffeescript.org) to render **javascript**.

To view the compiled versions you can select the carot drop down and click **View Compiled (language)**

![How to view compiled versions](//i.imgur.com/uCcB7Wp.gif)
>>>

## [before-after image comparator](//codepen.io/konstantinv/pen/LYPZbKY) *(codepen)*
![Image comparator demo](//i.imgur.com/Lf1Aqoe.gif)
----
##### html
```html
<div class='.before-after'>
	<img src='' />
	<img src='' />
</div>
```
##### css
```css
.before-after {
	position: relative;
	height: max-content;
	width: max-content;
}
.before-after img {
	top: 0;
	left: 0;
}

.before-after img:not(:first-of-type) {
	position: absolute;
}
```
##### js *(coffeescript)*
```coffee
beforeafter = document.querySelector '.before-after'
after = beforeafter.lastElementChild
mouse = (e) ->
	x  = (e.clientX  -  e.target.getBoundingClientRect().left) /  beforeafter.clientWidth  *  100
	after.style.clipPath  =  "inset(0 0 0 #{x}%)"
touch  = (e) ->
	x  = (e.touches[0].clientX  -  e.target.getBoundingClientRect().left) /  beforeafter.clientWidth  *  100
	after.style.clipPath  =  "inset(0 0 0 #{x}%)"
beforeafter.addEventListener 'touchmove', touch, passive: true
beforeafter.addEventListener 'mousemove', mouse
```